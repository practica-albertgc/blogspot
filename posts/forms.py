from django import forms
from .models import Post

class PostsForm(forms.Form):

    title = forms.CharField(max_length=5, required=True)
    body = forms.CharField(widget=forms.Textarea)

    class Meta:
        model: 'Post'