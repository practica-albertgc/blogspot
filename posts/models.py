from django.db import models

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=60)
    body = models.TextField(max_length=190)
    author = models.CharField(max_length=60, default='Albert')
    edition = models.IntegerField(default=1)
    year = models.IntegerField(default=2019)
    type = models.CharField(max_length=60, default='Libro')
    available = models.BooleanField(default=True)


    def __str__(self):
        return self.title