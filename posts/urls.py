from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'posts'
urlpatterns = [
    path('', views.store, name="posts.store"),
    path('results/<title>', views.showResults, name="results"),
    path('search', views.search, name="search")
]