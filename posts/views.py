from django.shortcuts import render, redirect
from .forms import PostsForm
from .models import Post

# Create your views here.
def store(request):

    if request.method == 'POST':
        form = PostsForm(request.POST)
        if form.is_valid():
            ''' post = Post(title=form.cleaned_data['title'], body=form.cleaned_data['body']) '''
            post = Post()
            post.title = form.cleaned_data['title']
            post.body = form.cleaned_data['body']
            post.save()

            return redirect('posts:posts.store')

    form = PostsForm()
    posts = Post.objects.all()
    return render(request, 'posts/form.html', {'form': form, 'posts': posts})

def showResults(request, title):

    result = Post.objects.get(title=title)

    return render(request, 'posts/result.html', {'result': result})

def search(request):

    if request.method == 'POST':
        title = request.POST['title']
        result = Post.objects.get(title=title)

    else:
        result = 'this is'

    return render(request, 'posts/search.html', {'result': result})